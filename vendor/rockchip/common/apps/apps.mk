

PRODUCT_COPY_FILES += \
	vendor/rockchip/common/apps/DeviceTest/lib/vm:system/xbin/vm \
	vendor/rockchip/common/apps/DeviceTest/lib/systemconfig:system/bin/systemconfig \
	vendor/rockchip/common/apps/DeviceTest/lib64/libserial_port.so:system/lib64/libserial_port.so \
	vendor/rockchip/common/apps/DeviceTest/lib/libserial_port.so:system/lib/libserial_port.so \
	vendor/rockchip/common/apps/DeviceTest/lib/libdrm_devicetest.so:system/lib/libdrm_devicetest.so \
	vendor/rockchip/common/apps/DeviceTest/lib64/libdrm_devicetest.so:system/lib64/libdrm_devicetest.so 

ifneq ($(strip $(TARGET_BOARD_PLATFORM_PRODUCT)), vr)
PRODUCT_PACKAGES += \
    RkApkinstaller  \
    userExperienceService
ifneq ($(strip $(TARGET_BOARD_PLATFORM)), sofia3gr)
PRODUCT_PACKAGES += \
    MediaFloat      \
endif

ifeq ($(strip $(TARGET_BOARD_PLATFORM)), rk3288)
PRODUCT_PACKAGES += \
    WinStartService \
    projectX
endif

ifeq ($(strip $(TARGET_BOARD_PLATFORM)), rk312x)
PRODUCT_PACKAGES += \
    RkVideoPlayer
else
ifeq (false, true) #($(strip $(TARGET_BOARD_PLATFORM_PRODUCT)), box)
PRODUCT_PACKAGES += \
    RkBoxVideoPlayer
else
ifeq ($(strip $(PRODUCT_BUILD_MODULE)), px5car)
PRODUCT_PACKAGES += \
	Rk3grVideoPlayer
else
PRODUCT_PACKAGES += \
    Rk4kVideoPlayer
endif
endif
endif
endif

ifeq ($(strip $(TARGET_BOARD_PLATFORM)), sofia3gr)
PRODUCT_PACKAGES += \
    Rk3grExplorer
else
ifeq ($(strip $(PRODUCT_BUILD_MODULE)), px5car)
PRODUCT_PACKAGES += \
    Rk3grExplorer
else
PRODUCT_PACKAGES += \
    RkExplorer
endif
endif
endif



ifeq ($(strip $(BOARD_HAS_STRESSTEST_APP)), true)
    PRODUCT_PACKAGES += \
    StressTest \
    DeviceTest
endif


ifeq ($(strip $(TARGET_BOARD_PLATFORM)), sofia3gr)
PRODUCT_PACKAGES += \
    ituxd\
    com.rockchip.alarmhelper \
    RFTest

#PRODUCT_COPY_FILES += \
#        $(LOCAL_PATH)/ituxd/lib/x86/libthermalJNI.so:system/lib/libthermalJNI.so
endif


ifeq ($(strip $(TARGET_BOARD_PLATFORM_PRODUCT)), stbvr)
PRODUCT_PACKAGES += \
		 RockSTBVRHome
endif

ifneq (,$(filter vr stbvr, $(TARGET_BOARD_PLATFORM_PRODUCT)))
PRODUCT_PACKAGES += \
			
endif

###########for box app ################
#ifeq ($(strip $(TARGET_BOARD_PLATFORM_PRODUCT)), box)
ifneq (,$(filter box stbvr, $(TARGET_BOARD_PLATFORM_PRODUCT)))
PRODUCT_PACKAGES += \
    eHomeMediaCenter_box	\
    Music	\
    SoundRecorder \
    DualScreenApk
  ifeq ($(strip $(BOARD_USE_LOW_MEM256)), true)
        PRODUCT_PACKAGES += \
              SimpleLauncher
  endif
endif
